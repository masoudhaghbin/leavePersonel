<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('login' , 'AuthController@apiLogin');
Route::post('logout' , 'AuthController@apiLogout');
//Route::post('operation/leave/add', 'OperationController@apiAddLeave');

Route::group(['prefix' => 'operation'], function (){
        Route::group(['prefix' => 'leave'], function (){
            Route::post('add', 'OperationController@apiAddLeave');
            Route::post('remove', 'OperationController@apiRemoveLeave');
            Route::get('list', 'OperationController@apiGetLeaveList');
        });
    });
