<?php


namespace App;


class LeaveSchedule
{
    public $date;
    public $days;

    public function __construct($date, $days)
    {
        $this->date = $date;
        $this->days = $days;
    }

    public function getDate(){
        return $this->date;
    }

    public function getDays(){
        return $this->days;
    }
}