<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    protected $table = 'leaves';

    public function personnel(){
        return $this->hasOne(Personnel::class, 'id', 'PersonnelFk');
    }

    public function reject(){
        if($this->rejectDate == null)
        {
            $this->rejectDate = Carbon::now();
            $this->save();
        }
    }

    public function scopeActive($query){
        return $query->whereNull('rejectDate');
    }

    public function scopeRejected($query){
        return $query->whereNotNull('rejectDate');
    }
}
