<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Boss extends Model
{
    protected $table = 'bosses';

    public function personnel(){
        return $this->hasMany(Personnel::class, 'BossFk', 'id');
    }
}
