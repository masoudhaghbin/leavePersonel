<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personnel extends Model
{
    protected $table = 'personnel';

    public function boss(){
        return $this->hasOne(Boss::class, 'id', 'BoosFk');
    }
}
