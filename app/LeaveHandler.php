<?php


namespace App;


class LeaveHandler
{
    public $personnel;
    public $leaveSchedule;

    public function __construct(Personnel $personnel, LeaveSchedule $leaveSchedule)
    {
        $this->personnel = $personnel;
        $this->leaveSchedule = $leaveSchedule;
    }

    public function addLeave(){
        $leave = new Leave();
        $leave->personnelFk = $this->personnel->id;
        $leave->leaveDate = $this->leaveSchedule->getDate();
        $leave->daysOff = $this->leaveSchedule->getDays();
        $leave->save();
        return $leave;
    }

    public static function removeLeave($leaveId){
        $leaveModel = Leave::findOrFail($leaveId);
        $leaveModel->reject();
        return $leaveModel;
    }
}