<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddLeaveRequest;
use App\Http\Requests\RemoveLeaveRequest;
use App\Leave;
use App\LeaveHandler;
use App\LeaveSchedule;
use App\Personnel;

class OperationController extends Controller
{
    public function apiAddLeave(AddLeaveRequest $request){
        $data = $request->validated();
        $personnel = Personnel::find($data['personnelId']);
        $leaveSchedule = new LeaveSchedule($data['leaveDate'], $data['leaveDays']);
        $leaveHandler = new LeaveHandler($personnel, $leaveSchedule);
        $addStatus = $leaveHandler->addLeave();
        //
        return response()->json([
            'status' => 200,
            'msg' => 'Leave Added Successfully'
        ]);
    }

    public function apiRemoveLeave(RemoveLeaveRequest $request){
        $data = $request->validated();
        $remove = LeaveHandler::removeLeave($data['leaveId']);
        return response()->json([
            'status' => 200,
            'msg' => 'Leave Removed Successfully'
        ]);
    }

    public function apiGetLeaveList(){
        return response()->json([
            'status' => 200,
            'data' => [
                'leaves' => [
                    'active' => Leave::active()->get(),
                    'rejected' => Leave::rejected()->get()
                ]
            ]
        ]);
    }
}
