<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\JWT;

class AuthController extends Controller
{
    /**
     * @param LoginRequest $request
     * @return mixed
     */
    public function apiLogin(LoginRequest $request){
        $data = $request->validated();
        if(Auth::attempt($data)){
            $user = Auth::user();
            $token = JWTAuth::fromUser($user);
//            dd(JWTauth::getToken());
            return response()->json([
                'FullName' => $user->name,
                'token' => $token
            ]);
        }
        else{
            return response()->json([
                'status' => 417,
                'msg' => 'Authentication Failed'
            ]);
        }
    }

    public function apiLogout(){
//        dd(JWTAuth::getToken());
//        JWTAuth::invalidate(JWTAuth::getToken());
//        Auth::logout();
        return response()->json([
            'status' => 200,
            'msg' => 'Logged Out Successfully!' // todo: load from MessageFactory
        ]);
    }
}
