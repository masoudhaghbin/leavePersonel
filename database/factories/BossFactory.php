<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Boss;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Boss::class, function (Faker $faker) {
    return [
        'FullName' => $faker->name
    ];
});
