<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Personnel;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Personnel::class, function (Faker $faker) {
    return [
        'FullName' => $faker->name
    ];
});
