<?php

use Illuminate\Database\Seeder;

class PersonnelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Personnel::class, 2)->create()->each(function($p) {
            $p->boss()->save(factory(App\Boss::class)->make());
        });
    }
}
